export default function detectLanguage() {
    // const lang = (navigator.language || navigator.userLanguage).toLowerCase();
    const lang = (navigator.language).toLowerCase();
    if (lang.startsWith('ru')) {
        return 'ru';
    }
    if (lang.startsWith('zh')) {
        return 'zh';
    }
    return 'en';
}
