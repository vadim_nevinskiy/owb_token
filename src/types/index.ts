export interface IDeviceType {
    id: number,
    size: number,
    type: string
}
