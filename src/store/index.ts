import {combineReducers, configureStore} from '@reduxjs/toolkit'
import mainSlice from "./reducers/main/mainSlice";
import cdnLocalizationSlice from "./reducers/cdnLocalization/cdnLocalizationSlices";

const rootReducer = combineReducers({
    main: mainSlice,
    cdnLocalization: cdnLocalizationSlice
});


export const store = configureStore({
    reducer: rootReducer
});


export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
