import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {DEVICE_TYPES, DEVICE_VERSION, LANGUAGES} from "../../../constants";
import {IDeviceType} from "../../../types";


interface IInitialStateTypes {
    language: string
    calculatedDeviceType: IDeviceType
    screenWidth: number
    zoomOutForRetina: boolean
    deviceVersion: string
}
const initialState: IInitialStateTypes = {
    language: LANGUAGES.RU,
    calculatedDeviceType: {
        id: 0,
        size: 0,
        type: DEVICE_TYPES.DESKTOP_1920
    },
    screenWidth: 0,
    zoomOutForRetina: false,
    deviceVersion: DEVICE_VERSION.DESKTOP,
}

const mainSlice = createSlice({
    name: 'main',
    initialState: initialState,
    reducers: {
        setLanguage(state: IInitialStateTypes = initialState, action: PayloadAction<string>): void {
            state.language = action.payload
        },
        setDeviceType(state: IInitialStateTypes = initialState, action: PayloadAction<IDeviceType>): void {
            state.calculatedDeviceType = action.payload
        },
        setScreenWidth(state: IInitialStateTypes = initialState, action: PayloadAction<number>): void {
            state.screenWidth = action.payload
        },
        setZoomOutForRetina(state: IInitialStateTypes = initialState, action: PayloadAction<boolean>): void {
            state.zoomOutForRetina = action.payload
        },
        setDeviceVersion(state: IInitialStateTypes = initialState, action: PayloadAction<string>): void {
            state.deviceVersion = action.payload
        },
    }
})


export const {
    setLanguage,
    setDeviceType,
    setScreenWidth,
    setZoomOutForRetina,
    setDeviceVersion,
} = mainSlice.actions

export default mainSlice.reducer
