import {createSlice, PayloadAction} from '@reduxjs/toolkit';



interface IInitialStateTypes {
    loaded: boolean
    value: any | null
}



const initialState: IInitialStateTypes = {
    loaded: false,
    value: null,
}


const cdnLocalizationSlice = createSlice({
    name: 'cdnLocalization',
    initialState: initialState,
    reducers: {
        setLocalization(state: IInitialStateTypes = initialState, action: PayloadAction<any>): void {
            state.loaded = true
            state.value = action.payload
        },
    }
})


export const {
    setLocalization,
} = cdnLocalizationSlice.actions
export default cdnLocalizationSlice.reducer
