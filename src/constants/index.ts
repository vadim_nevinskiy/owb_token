export enum LANGUAGES {
    EN = 'en',
    PL = 'pl',
    RU = 'ru',
    UA = 'ua',
    ZH = 'zh',
};

export enum DEVICE_TYPES {
    PHONE_MINI_320 = 'PHONE_MINI',
    PHONE_375 = 'PHONE',
    PHONE_414 = 'PHONE_MAX',
    PHONE_428 = 'PHONE_ULTRA',
    TABLET_768 = 'TABLET',
    TABLET_LANDSCAPE_900 = 'TABLET_LANDSCAPE',
    TABLET_BIG_LANDSCAPE_1024 = 'TABLET_BIG_LANDSCAPE',
    TABLET_BIG_LANDSCAPE_PORTRAIT_1366 = 'TABLET_BIG_LANDSCAPE_PORTRAIT_1366',
    DESKTOP_1920 = 'DESKTOP',
    DESKTOP_ULTRA_2900 = 'DESKTOP_ULTRA',
};
export enum DEVICE_VERSION {
    DESKTOP = 'DESKTOP',
    TABLET = 'TABLET',
    PHONE = 'PHONE',
};

export const LOCALIZATION_CDN = "https://cdn.simplelocalize.io/768ff40ec2834a6bb7ab674cc1be9adf/_latest/_index";
