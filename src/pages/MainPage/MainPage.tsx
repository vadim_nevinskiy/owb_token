import React, {useEffect, useRef, useState} from 'react';
import {isMacOs, isMobile as isMobileDetect, isTablet as isTabletDetect  } from 'react-device-detect';
import classes from "./MainPage.module.scss";
import classNames from "classnames";
import {useDispatch, useSelector} from "react-redux";
import {
    setDeviceType,
    setScreenWidth,
    setZoomOutForRetina,
    setDeviceVersion
} from '../../store/reducers/main/mainSlice';
import getDeviceTypeService from "../../services/getDeviceTypeService";
import {RootState} from "../../store";
import {DEVICE_VERSION} from "../../constants";
import calculateDeviceVersionService from "../../services/calculateDeviceVersionService";
import useTranslation from "../../hooks/useTranslation";
import {
    Header,
    ScreenOne,
    ScreenTwo,
    ScreenThree,
    ScreenFour,
    ScreenFive,
    ScreenSix,
    ScreenSeven
} from "../../components";
import {BottomMenu, Footer} from "../../components/common";


interface IProps {

}
const MainPage:React.FC<IProps> = ({}) => {
    const dispatch = useDispatch();
    const translation = useTranslation('TokenLanding');
    const blockRef = useRef<HTMLDivElement>(null);
    const calcDeviceType = useSelector((state: RootState) => state.main.calculatedDeviceType);
    const screenWidth = useSelector((state: RootState) => state.main.screenWidth);
    const language = useSelector((state: RootState) => state.main.language);
    const zoomOutForRetina = useSelector((state: RootState) => state.main.zoomOutForRetina);
    const deviceVersion = useSelector((state: RootState) => state.main.deviceVersion);



    const [isTablet, setIsTablet] = useState(false);
    const [isPhone, setIsPhone] = useState(false);
    const [isDesktop, setIsDesktop] = useState(false);
    useEffect(() => {
        if(deviceVersion === DEVICE_VERSION.TABLET) {
            setIsTablet(true);
            setIsPhone(false);
            setIsDesktop(false);
        } else if(deviceVersion === DEVICE_VERSION.PHONE) {
            setIsTablet(false);
            setIsPhone(true);
            setIsDesktop(false);
        }else{
            setIsTablet(false);
            setIsPhone(false);
            setIsDesktop(true);
        }
    }, [deviceVersion, calcDeviceType])



    //Set one of three device version (DESKTOP, TABLET or PHONE)
    useEffect(() => {
        const deviceVersion = calculateDeviceVersionService(calcDeviceType);
        dispatch(setDeviceVersion(deviceVersion))
    }, [calcDeviceType]);

    const updateDimensions = () => {
        const device = getDeviceTypeService();
        dispatch(setDeviceType(device.deviceType));
        dispatch(setScreenWidth(device.calcWidth));
        dispatch(setZoomOutForRetina(isMacOs));
    }

    const [scrollingPosition, setScrollingPosition] = useState(0);
    const whenHideElements = 10;
    const scrollHandler = (e: any) => {
        const scrollTop = Math.ceil(e.target.scrollTop);
        setScrollingPosition(scrollTop);
    };


    useEffect(() => {
        updateDimensions();
        window.addEventListener('resize', updateDimensions);
        blockRef.current?.addEventListener('scroll', scrollHandler)


        return () => {
            window.removeEventListener('resize', updateDimensions);
            blockRef.current?.removeEventListener('scroll', scrollHandler)
        };
    }, []);


    return (
        <div className={classNames({
            [classes.MainPage]: true,
        })}
             ref={blockRef}
        >
            <Header position={scrollingPosition} isPhone={isPhone} isTablet={isTablet} />
            {/*<div style={{position: 'fixed', left: 50, bottom: 50, background: 'yellow', padding: 10, zIndex: 100 }}>*/}
            {/*    <div>scrollingPosition = {scrollingPosition}</div>*/}
            {/*    <div>screenWidth = {screenWidth}</div>*/}
            {/*    <div>deviceVersion = {deviceVersion}</div>*/}
            {/*</div>*/}
            <ScreenOne />
            <ScreenTwo />
            <ScreenThree />
            <ScreenFour />
            <ScreenFive />
            <ScreenSix />
            <ScreenSeven />
            <BottomMenu />

            <Footer />
        </div>
    );
};

export default MainPage;
