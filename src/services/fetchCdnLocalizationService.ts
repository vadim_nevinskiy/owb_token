import * as api from "../api/api";
import {LOCALIZATION_CDN} from "../constants";

async function fetchCdnLocalizationService() {
    const url = LOCALIZATION_CDN;
    const result = await api
        .getDataFromUrl(url)
        .then((response: any) => {
            return response
        })
        .catch(console.error);
    return result;
}

export default fetchCdnLocalizationService;
