import {device} from "../data/devices";

const getDeviceTypeService = () => {
    // const innerWidth = window.innerWidth;//Width inside block with scrollbar
    const clientWidth = document.documentElement.clientWidth; //Width inside block without scrollbar
    const calcWidth = clientWidth; // For switching calculating width

    let deviceType = device[8];
    calcWidth <= device[0].size
        ? deviceType = device[0]
        : calcWidth > device[0].size && calcWidth <= device[1].size
        ? deviceType = device[1]
        : calcWidth > device[1].size && calcWidth <= device[2].size
            ? deviceType = device[2]
            : calcWidth > device[2].size && calcWidth <= device[3].size
                ? deviceType = device[3]
                : calcWidth > device[3].size && calcWidth <= device[4].size
                    ? deviceType = device[4]
                    : calcWidth > device[4].size && calcWidth <= device[5].size
                        ? deviceType = device[5]
                        : calcWidth > device[5].size && calcWidth <= device[6].size
                            ? deviceType = device[6]
                            : calcWidth > device[6].size && calcWidth <= device[7].size
                                ? deviceType = device[7]
                                : calcWidth > device[7].size && calcWidth <= device[8].size
                                    ? deviceType = device[8]
                                    : calcWidth > device[8].size && calcWidth <= device[9].size
                                        ? deviceType = device[9]
                                        : deviceType = device[8];

    return {deviceType, calcWidth};
}

export default getDeviceTypeService;
