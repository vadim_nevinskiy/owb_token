import {DEVICE_TYPES, DEVICE_VERSION} from "../constants";
import {IDeviceType} from "../types";


const calculateDeviceVersionService = (calcDeviceType: IDeviceType) => {
    let deviceVersion = '';


    calcDeviceType.type === DEVICE_TYPES.DESKTOP_ULTRA_2900 ||
    calcDeviceType.type === DEVICE_TYPES.DESKTOP_1920 ||
    calcDeviceType.type === DEVICE_TYPES.TABLET_BIG_LANDSCAPE_PORTRAIT_1366 ||
    calcDeviceType.type === DEVICE_TYPES.TABLET_BIG_LANDSCAPE_1024 || //Перенес viewport 1024 и 900 в категорию десктопов, планшет считаем только от 768
    calcDeviceType.type === DEVICE_TYPES.TABLET_LANDSCAPE_900
        ? deviceVersion = DEVICE_VERSION.DESKTOP
        : calcDeviceType.type === DEVICE_TYPES.TABLET_768
        ? deviceVersion = DEVICE_VERSION.TABLET
        : calcDeviceType.type === DEVICE_TYPES.PHONE_428 ||
        calcDeviceType.type === DEVICE_TYPES.PHONE_414 ||
        calcDeviceType.type === DEVICE_TYPES.PHONE_375 ||
        calcDeviceType.type === DEVICE_TYPES.PHONE_MINI_320
            ? deviceVersion = DEVICE_VERSION.PHONE
            : deviceVersion = DEVICE_VERSION.DESKTOP;

    return deviceVersion;
};

export default calculateDeviceVersionService;

