import {DEVICE_TYPES} from "../constants";
export const device = [
    { id: 0, size: 320, type: DEVICE_TYPES.PHONE_MINI_320 },
    { id: 1, size: 375, type: DEVICE_TYPES.PHONE_375 }, //From design mobile view
    { id: 2, size: 414, type: DEVICE_TYPES.PHONE_414 }, //From design mobile view
    { id: 3, size: 428, type: DEVICE_TYPES.PHONE_428 }, //From design mobile view
    { id: 4, size: 768, type: DEVICE_TYPES.TABLET_768 }, //From design tablet view
    { id: 5, size: 900, type: DEVICE_TYPES.TABLET_LANDSCAPE_900 },
    { id: 6, size: 1024, type: DEVICE_TYPES.TABLET_BIG_LANDSCAPE_1024 },
    { id: 7, size: 1366, type: DEVICE_TYPES.TABLET_BIG_LANDSCAPE_PORTRAIT_1366 },
    { id: 8, size: 1920, type: DEVICE_TYPES.DESKTOP_1920 }, //From design desktop view
    { id: 9, size: 2900, type: DEVICE_TYPES.DESKTOP_ULTRA_2900 },
];
