export const data = [{
    "grad": "1",
    "value_1": 1,
    "value_2": 2,
    "value_3": 3,
    "value_4": 4,
    "value_5": 5
},{
    "grad": "2",
    "value_1": 2,
    "value_2": 3,
    "value_3": 4,
    "value_4": 5,
    "value_5": 6
},{
    "grad": "3",
    "value_1": 3,
    "value_2": 4,
    "value_3": 5,
    "value_4": 6,
    "value_5": 7
},{
    "grad": "4",
    "value_1": 54,
    "value_2": 64,
    "value_3": 35,
    "value_4": 78,
    "value_5": 84
},{
    "grad": "5",
    "value_1": 57,
    "value_2": 59,
    "value_3": 67,
    "value_4": 86,
    "value_5": 100
},{
    "grad": "6",
    "value_1": 73,
    "value_2": 64,
    "value_3": 68,
    "value_4": 123,
    "value_5": 145
},{
    "grad": "7",
    "value_1": 173,
    "value_2": 164,
    "value_3": 168,
    "value_4": 223,
    "value_5": 245
},{
    "grad": "8",
    "value_1": 273,
    "value_2": 264,
    "value_3": 268,
    "value_4": 323,
    "value_5": 345
},{
    "grad": "9",
    "value_1": 373,
    "value_2": 364,
    "value_3": 368,
    "value_4": 423,
    "value_5": 445
},{
    "grad": "9",
    "value_1": 473,
    "value_2": 464,
    "value_3": 468,
    "value_4": 523,
    "value_5": 545
},{
    "grad": "10",
    "value_1": 573,
    "value_2": 564,
    "value_3": 568,
    "value_4": 623,
    "value_5": 645
},{
    "grad": "11",
    "value_1": 673,
    "value_2": 664,
    "value_3": 668,
    "value_4": 723,
    "value_5": 745
},{
    "grad": "12",
    "value_1": 773,
    "value_2": 764,
    "value_3": 768,
    "value_4": 823,
    "value_5": 845
},];
