export const languages = [
    {language: 'en', name: 'English'},
    {language: 'pl', name: 'Polski'},
    {language: 'ru', name: 'Русский'},
    {language: 'ua', name: 'Українська'},
    {language: 'zh', name: 'Chinese'},
];
