import { useSelector } from 'react-redux';
import translations from '../data/localization.json';
import {RootState} from "../store";
import {LOCALIZATION_CDN} from "../constants";


export default function useTranslation(namespace: string) {
    interface IStringIndex extends Record<string, any> {};
    const localization: IStringIndex = translations;
    const lang = useSelector((state: RootState) => state.main.language);
    const cdnLocalization = useSelector((state: RootState) => state.cdnLocalization);
    const cdnUrl = LOCALIZATION_CDN;
    // if (!cdnUrl || !cdnLocalization.loaded) {
    //     return (key: string) => localization[lang][namespace][key];
    // }
    // return (key: string) => cdnLocalization.value[lang][namespace][key];

    //TODO После добавления переводов в облачный сервис, раскомментировать верхний код cdnUrl
    return (key: string) => localization[lang][namespace][key];
}
