import React from 'react';
import classes from "./ScreenSix.module.scss";
import classNames from "classnames";
import BgLineLeft from "./BgLineLeft/BgLineLeft";
import BgLineRight from "./BgLineRight/BgLineRight";
import {ChartToken, IndentAdjustmentBlockForAnchors, MainTitle, SubTitle} from "../common";
import useTranslation from "../../hooks/useTranslation";
import {Tokens} from "../common/IMAGES";



interface IProps {

}
const ScreenSix:React.FC<IProps> = ({}) => {
    const translation = useTranslation('TokenLanding');


    return (
        <div
            className={classNames({
                [classes.ScreenSix]: true
            })}
        >
            <IndentAdjustmentBlockForAnchors topPosition={100} blockId={"ScreenSix"} />
            <div className={classNames({
                [classes.ScreenSix__wrapper]: true
            })}>
                <BgLineLeft />
                <BgLineRight />
                <div className={classNames({
                    [classes.ScreenSix__wrapper__tokenImg]: true
                })}>
                    <Tokens />
                </div>
                <SubTitle title={translation("ScreenSixSubTitle")} />
                <MainTitle title={translation("ScreenSixMainTitle")} />
                <ChartToken />
            </div>
            {/*<div className={classNames({*/}
            {/*    [classes.ScreenSix__BottomRadiusBlock]: true*/}
            {/*})} />*/}
        </div>
    );
};

export default ScreenSix;
