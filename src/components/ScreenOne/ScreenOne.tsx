import React from 'react';
import classes from "./ScreenOne.module.scss";
import classNames from "classnames";
import {CoinImage, ElizaFinFlatImage, OWBLogo} from "../common/IMAGES";
import {GreenBtnTimer, IndentAdjustmentBlockForAnchors} from "../common";


interface IProps {

}
const ScreenOne:React.FC<IProps> = ({}) => {
    return (
        <div
            className={classNames({
                [classes.ScreenOne]: true
            })}
        >
            <IndentAdjustmentBlockForAnchors topPosition={-100} blockId={"ScreenOne"} />
            <div className={classNames({
                [classes.ScreenOne__Wrapper]: true
            })}>
                <OWBLogo />
                <ElizaFinFlatImage />
                <div className={classNames({
                    [classes.ScreenOne__Wrapper__Coin]: true
                })}>
                    <CoinImage />
                </div>

                <GreenBtnTimer />
            </div>
        </div>
    );
};

export default ScreenOne;
