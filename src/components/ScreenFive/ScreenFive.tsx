import React from 'react';
import classes from "./ScreenFive.module.scss";
import classNames from "classnames";
import {IndentAdjustmentBlockForAnchors, MainTitle, RoadMap, SubTitle} from "../common";
import useTranslation from "../../hooks/useTranslation";
import {Plane} from "../common/IMAGES";

interface IProps {

}
const ScreenFive:React.FC<IProps> = ({}) => {
    const translation = useTranslation('TokenLanding');

    return (
        <div
            className={classNames({
                [classes.ScreenFive]: true
            })}
        >
            <IndentAdjustmentBlockForAnchors topPosition={-100} blockId={"ScreenFive"} />
            <div className={classNames({
                [classes.ScreenFive__TopRadiusBlock]: true
            })} />
            <SubTitle title={translation("ScreenFiveSubTitle")} />
            <MainTitle title={translation("ScreenFiveMainTitle")} />

            <div className={classNames({
                [classes.ScreenFive__RoadMap]: true
            })}>
                <div className={classNames({
                    [classes.ScreenFive__RoadMap__PlaneImg]: true
                })}>
                    <Plane />
                </div>
                <RoadMap />
            </div>

        </div>
    );
};

export default ScreenFive;
