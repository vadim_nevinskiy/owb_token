import React from 'react';
import classNames from "classnames";
import classes from "./ScreenSeven.module.scss";
import {ChartVesting, MainTitle, SubTitle} from "../common";
import useTranslation from "../../hooks/useTranslation";


interface IProps {

}
const ScreenSeven:React.FC<IProps> = ({}) => {
    const translation = useTranslation('TokenLanding');


    return (
        <div className={classNames({
            [classes.ScreenSeven]: true
        })}>
            <div className={classNames({
                [classes.ScreenSeven__TopRadiusBlock]: true
            })} />
            <SubTitle title={translation("ScreenSevenSubTitle")} />
            <MainTitle title={translation("ScreenSevenMainTitle")} />

            <ChartVesting />
            {/*<div className={classNames({*/}
            {/*    [classes.ScreenSeven__wrapper]: true*/}
            {/*})}>*/}
            {/*    <SubTitle title={translation("ScreenSevenSubTitle")} />*/}
            {/*    <MainTitle title={translation("ScreenSevenMainTitle")} />*/}
            {/*</div>*/}
        </div>
    );
};

export default ScreenSeven;
