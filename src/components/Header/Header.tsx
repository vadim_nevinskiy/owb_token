import React, {useEffect, useState} from 'react';
import classes from "./Header.module.scss";
import classNames from "classnames";
import {BurgerBtn, LanguageSwitcher, Logo, Menu} from "../common";



interface IProps {
    position: number
    isPhone: boolean
    isTablet: boolean
}
const Header:React.FC<IProps> = ({position, isPhone, isTablet}) => {
    const [isFloating, setIsFloating] = useState(false)
    const [burgerMenuActive, setBurgerMenuActive] = useState(false)

    useEffect(() => {
        if(position > 100) {
            setIsFloating(true);
        } else {
            setIsFloating(false);
        }
    }, [position])

    const toggleMenu = () => {
        setBurgerMenuActive(!burgerMenuActive);
    }

    return (
        <div className={classNames({
            [classes.Header]: true,
            [classes.Header__floating]: isFloating || isPhone || isTablet
        })}
        >
            <div className={classNames({
                [classes.Header__wrapper]: true,
                [classes['Header__wrapper-mobile']]: isPhone || isTablet
            })}>
                {
                    (isPhone || isTablet) &&
                        <BurgerBtn burgerMenuActive={burgerMenuActive} toggleMenu={toggleMenu} />
                }
                {
                    (isPhone || isTablet) &&
                    <div className={classNames({
                        [classes.Header__wrapper__mobilePanel]: true,
                        [classes.Header__wrapper__mobilePanel_active]: burgerMenuActive,
                    })}>
                        <Menu mobileVersion={true} />
                    </div>
                }
                <Logo />
                {
                    (!isPhone && !isTablet) &&
                    <Menu mobileVersion={false} />
                }
                <LanguageSwitcher />
            </div>

        </div>
    );



};

export default Header;
