import React, {useEffect, useRef, useState} from 'react';
import classes from "./ScreenThree.module.scss";
import classNames from "classnames";
import {Content, GreenButton, IndentAdjustmentBlockForAnchors, MainTitle, SubTitle, Timer} from "../common";
import useTranslation from "../../hooks/useTranslation";
import {Drone} from "../common/IMAGES";


interface IProps {

}
const ScreenThree:React.FC<IProps> = ({}) => {
    const translation = useTranslation('TokenLanding');
    const circleBlock = useRef<HTMLDivElement>(null);

    const [width, setWidth] = useState<number | undefined>(0);
    const [paddingTop, setPaddingTop] = useState<number>(200);
    const updateHeight = () => {
        const width = circleBlock.current?.clientWidth;
        setWidth(width);
        if(width){
            if(width < 768) setPaddingTop( 50)
            else if(width < 414) setPaddingTop( 30)
        }

    }
    useEffect(() => {
        updateHeight();
        window.addEventListener('resize', updateHeight);


        return () => {
            window.removeEventListener('resize', updateHeight);
        };
    }, []);

    const clickBtn = () => {
        alert('clicked REGISTER')
    }

    return (
        <div
            className={classNames({
                [classes.ScreenThree]: true
            })}
        >
            <IndentAdjustmentBlockForAnchors topPosition={0} blockId={"ScreenThree"} />
            <div className={classNames({
                [classes.ScreenThree__Wrapper]: true
            })}>
                <div className={classNames({
                    [classes.ScreenThree__Wrapper__BottomBg]: true
                })}>
                    <div
                        ref={circleBlock}
                        className={classNames({
                            [classes.ScreenThree__Wrapper__BottomBg__BottomBlock]: true
                        })}
                        style={{height: (width ? width - paddingTop : 0)}}
                    >
                        <SubTitle title={translation("ThirdScreenSubTitle")} />
                        <MainTitle title={translation("ThirdScreenMainTitle")} />

                        <div className={classNames({
                            [classes.ScreenThree__Wrapper__BottomBg__BottomBlock__Drone]: true
                        })}>
                            <Drone />
                        </div>

                        <div className={classNames({
                            [classes.ScreenThree__Wrapper__BottomBlock__ContentBlock]: true
                        })}>
                            <Content html={translation("ThirdScreenContent")} />
                        </div>


                        <div className={classNames({
                            [classes.ScreenThree__Wrapper__BottomBg__BottomBlock__Timer]: true
                        })}>
                            <Timer />
                        </div>
                        <div className={classNames({
                            [classes.ScreenThree__Wrapper__BottomBg__BottomBlock__CenterBtn]: true
                        })}>
                            <GreenButton title={translation("RegisterButton")} clickBtn={clickBtn} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ScreenThree;
