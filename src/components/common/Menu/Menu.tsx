import React from 'react';
import classes from "./Menu.module.scss";
import classNames from "classnames";
import useTranslation from "../../../hooks/useTranslation";

interface IProps {
    mobileVersion: boolean
    isBottomMenu?: boolean
}
const Menu:React.FC<IProps> = ({mobileVersion, isBottomMenu = false}) => {
    const translation = useTranslation('TokenLanding');
    const menu = [
        {name: translation("nft"), anchor: 'ScreenOne'},
        {name: translation("coc"), anchor: 'ScreenTwo'},
        {name: translation("token"), anchor: 'ScreenThree'},
        {name: translation("updates"), anchor: 'ScreenFour'},
        {name: translation("roadmap"), anchor: 'ScreenFive'},
        {name: translation("team"), anchor: 'ScreenSix'},
    ]

    return (
        <div className={classNames({
            [classes.Menu]: true,
            [classes.Menu__isBottomMenu]: isBottomMenu,
            [classes.Menu__mobileVersion]: mobileVersion
        })}>
            {
                menu.map((item, _idx) => {
                    return (
                        <a
                            href={`#${item.anchor}`}
                            key={_idx} className={classNames({
                                [classes.Menu__item]: true
                            })}
                        >
                            {item.name}
                        </a>
                        // <div key={_idx} className={classNames({
                        //     [classes.Menu__item]: true
                        // })}>
                        //     {item.name}
                        // </div>
                    )
                })
            }

        </div>
    );
};

export default Menu;
