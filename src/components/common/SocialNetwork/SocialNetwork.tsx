import React from 'react';
import classes from "./SocialNetwork.module.scss";
import classNames from "classnames";


interface IProps {

}
const SocialNetwork:React.FC<IProps> = ({}) => {
    return (
        <div className={classNames({
            [classes.SocialNetwork]: true
        })}>
            <a className={classNames({
                [classes.SocialNetwork__icon]: true,
                [classes.SocialNetwork__icon__disabled]: true,
                [classes.SocialNetwork__icon__ds]: true
            })} />
            <a className={classNames({
                [classes.SocialNetwork__icon]: true,
                [classes.SocialNetwork__icon__disabled]: true,
                [classes.SocialNetwork__icon__tw]: true
            })} />
            <a className={classNames({
                [classes.SocialNetwork__icon]: true,
                [classes.SocialNetwork__icon__disabled]: true,
                [classes.SocialNetwork__icon__tg]: true
            })} />
        </div>
    );
};

export default SocialNetwork;
