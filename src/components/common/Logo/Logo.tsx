import React from 'react';
import classes from './Logo.module.scss';
import classNames from "classnames";


const Logo = () => {
    return (
        <div className={classNames({
            [classes.Logo]: true
        })}/>
    );
};

export default Logo;
