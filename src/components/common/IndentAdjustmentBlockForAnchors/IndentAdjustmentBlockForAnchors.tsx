import React from 'react';
import classes from "./IndentAdjustmentBlockForAnchors.module.scss";
import classNames from "classnames";


interface IProps {
    topPosition: number
    blockId: string
}
const IndentAdjustmentBlockForAnchors:React.FC<IProps> = ({topPosition, blockId}) => {
    return (
        <div
            id={blockId}
            className={classNames({
                [classes.IndentAdjustmentBlockForAnchors]: true
            })}
            style={{top: topPosition}}
        />
    );
};

export default IndentAdjustmentBlockForAnchors;
