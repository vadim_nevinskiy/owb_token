import React from 'react';
import classes from "./ElizaFinFlatImage.module.scss";
import classNames from "classnames";


const ElizaFinFlatImage = () => {
    return (
        <div className={classNames({
            [classes.ElizaFinFlatImage]: true
        })} />
    );
};

export default ElizaFinFlatImage;
