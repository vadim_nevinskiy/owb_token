import React from 'react';
import classes from "./Plane.module.scss";
import classNames from "classnames";

interface IProps {

}
const Plane:React.FC<IProps> = ({}) => {
    return (
        <div className={classNames({
            [classes.Plane]: true
        })} />
    );
};

export default Plane;
