import React from 'react';
import classes from "./Tokens.module.scss";
import classNames from "classnames";




interface IProps {

}
const Tokens:React.FC<IProps> = ({}) => {
    return (
        <div className={classNames({
            [classes.Tokens]: true
        })}/>
    );
};

export default Tokens;
