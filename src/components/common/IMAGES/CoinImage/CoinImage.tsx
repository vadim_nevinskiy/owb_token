import React from 'react';
import classes from "./CoinImage.module.scss";
import classNames from "classnames";



const CoinImage = () => {
    return (
        <div className={classNames({
            [classes.CoinImage]: true
        })} />
    );
};

export default CoinImage;
