import React from 'react';
import classes from "./SmallCoinImage.module.scss";
import classNames from "classnames";


interface IProps {

}
const SmallCoinImage:React.FC<IProps> = ({}) => {
    return (
        <div className={classNames({
            [classes.SmallCoinImage]: true
        })} />
    );
};

export default SmallCoinImage;
