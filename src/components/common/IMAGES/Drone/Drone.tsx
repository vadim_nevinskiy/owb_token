import React from 'react';
import classes from "./Drone.module.scss";
import classNames from "classnames";

interface IProps {

}
const Drone:React.FC<IProps> = ({}) => {
    return (
        <div className={classNames({
            [classes.Drone]: true
        })} />
    );
};

export default Drone;
