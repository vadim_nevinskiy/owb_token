import React from 'react';
import classNames from "classnames";
import classes from "./OWBLogo.module.scss";

const OWBLogo = () => {
    return (
        <div className={classNames({
            [classes.OWBLogo]: true
        })} />
    );
};

export default OWBLogo;
