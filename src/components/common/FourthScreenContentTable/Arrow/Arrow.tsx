import React from 'react';
import classNames from "classnames";
import classes from "./Arrow.module.scss";


interface IProps {
    className: string
}
const Arrow:React.FC<IProps> = ({className}) => {
    return (
        <div className={classNames({
            [classes.Arrow]: true,
            [classes[`Arrow_${className}`]]: true
        })}/>
    );
};

export default Arrow;
