import React from 'react';
import classes from "./Icon.module.scss";
import classNames from "classnames";

interface IProps {
    classname: string
}
const Icon:React.FC<IProps> = ({classname}) => {
    return (
        <div className={classNames({
            [classes.Icon]: true,
            [classes[`Icon_${classname}`]]: true,
        })}/>
    );
};

export default Icon;
