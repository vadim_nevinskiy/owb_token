import React from 'react';
import classes from "./IconTitle.module.scss";
import classNames from "classnames";

interface IProps {
    title: string
    classname?: string
}
const IconTitle:React.FC<IProps> = ({title, classname}) => {


    return (
        <div className={classNames({
            [classes.IconTitle]: true
        })}>
            {title}
        </div>
    );
};

export default IconTitle;
