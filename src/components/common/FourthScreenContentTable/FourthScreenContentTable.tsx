import React from 'react';
import classes from "./FourthScreenContentTable.module.scss";
import classNames from "classnames";
import Icon from "./icon/Icon";
import IconTitle from "./iconTitle/IconTitle";
import {SmallCoinImage} from "../IMAGES";
import useTranslation from "../../../hooks/useTranslation";
import Arrow from "./Arrow/Arrow";


interface IProps {

}
const FourthScreenContentTable:React.FC<IProps> = ({}) => {
    const translation = useTranslation('TokenLanding');

    return (
        <div className={classNames({
            [classes.FourthScreenContentTable]: true
        })}>
            <div className={classNames({
                [classes.FourthScreenContentTable__LeftColumn]: true
            })}>
                <div className={classNames({
                    [classes.FourthScreenContentTable__LeftColumn__IconBlock]: true,
                    [classes.FourthScreenContentTable__LeftColumn__IconBlock_lt]: true
                })}>
                    <Icon classname={'coins'} />
                    <IconTitle title={translation("freeToEarn")} />
                    <Arrow className={"LT"} />
                </div>
                <div className={classNames({
                    [classes.FourthScreenContentTable__LeftColumn__IconBlock]: true,
                    [classes.FourthScreenContentTable__LeftColumn__IconBlock_lb]: true
                })}>
                    <Icon classname={'market'} />
                    <IconTitle title={translation("market")} />
                    <Arrow className={"LB"} />
                </div>
            </div>
            <div className={classNames({
                [classes.FourthScreenContentTable__Logo]: true
            })}>
                <SmallCoinImage />
            </div>
            <div className={classNames({
                [classes.FourthScreenContentTable__RightColumn]: true
            })}>
                <div className={classNames({
                    [classes.FourthScreenContentTable__RightColumn__IconBlock]: true,
                    [classes.FourthScreenContentTable__RightColumn__IconBlock_rt]: true
                })}>
                    <Arrow className={"RT"} />
                    <IconTitle title={translation("collectNFT")} />
                    <Icon classname={'collect'} />
                </div>
                <div className={classNames({
                    [classes.FourthScreenContentTable__RightColumn__IconBlock]: true,
                    [classes.FourthScreenContentTable__RightColumn__IconBlock_rb]: true
                })}>
                    <Arrow className={"RB"} />
                    <IconTitle title={translation("ownTheLands")} />
                    <Icon classname={'lands'} />
                </div>
            </div>
        </div>
    );
};

export default FourthScreenContentTable;
