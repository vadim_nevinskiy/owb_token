import React from 'react';
import classNames from "classnames";
import classes from "./ScreenTwoContentTable.module.scss";


interface IColumn {
    text_1: string
    text_2: string
}
interface IProps {
    leftColumn: IColumn
    rightColumn: IColumn
}
const ScreenTwoContentTable:React.FC<IProps> = ({leftColumn, rightColumn}) => {
    return (
        <div className={classNames({
            [classes.ScreenTwoContentTable]: true
        })}>
            <div>
                <div>
                    {leftColumn.text_1}
                </div>
                <div>
                    {leftColumn.text_2}
                </div>
            </div>
            <div>
                <div>
                    {rightColumn.text_1}
                </div>
                <div>
                    {rightColumn.text_2}
                </div>
            </div>
        </div>
    );
};

export default ScreenTwoContentTable;
