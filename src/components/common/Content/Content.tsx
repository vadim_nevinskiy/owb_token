import React from 'react';
import classes from "./Content.module.scss";
import classNames from "classnames";

interface IProps {
    html: string
}
const Content:React.FC<IProps> = ({html}) => {
    return (
        <div
            className={classNames({
                [classes.Content]: true
            })}
            dangerouslySetInnerHTML={{__html: html}}
        />
    );
};

export default Content;
