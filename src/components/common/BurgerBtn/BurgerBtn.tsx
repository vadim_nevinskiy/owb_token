import React from 'react';
import classes from './BurgerBtn.module.scss';
import classNames from "classnames";

interface IProps {
    burgerMenuActive: boolean
    toggleMenu: () => void
}
const BurgerBtn:React.FC<IProps> = ({burgerMenuActive, toggleMenu}) => {


    return (
        <div
            className={classNames({
                [classes.BurgerBtn]: true,
                [classes.BurgerBtn_active]: burgerMenuActive
            })}
            onClick={toggleMenu}
        >
            <div className={classNames({
                [classes.BurgerBtn__sprite]: true,
                [classes.BurgerBtn__sprite_one]: true,
                [classes.BurgerBtn__sprite_one_active]: burgerMenuActive
            })} />
            <div className={classNames({
                [classes.BurgerBtn__sprite]: true,
                [classes.BurgerBtn__sprite_two]: true,
                [classes.BurgerBtn__sprite_two_active]: burgerMenuActive
            })} />
            <div className={classNames({
                [classes.BurgerBtn__sprite]: true,
                [classes.BurgerBtn__sprite_three]: true,
                [classes.BurgerBtn__sprite_three_active]: burgerMenuActive
            })} />
        </div>
    );
};

export default BurgerBtn;
