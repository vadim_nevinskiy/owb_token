import React from 'react';
import classNames from "classnames";
import classes from "./MainTitle.module.scss";


interface IProps {
    title: string
}
const MainTitle: React.FC<IProps> = ({title}) => {
    return (
        <div className={classNames({
            [classes.MainTitle]: true
        })}>
            {title}
        </div>
    );
};

export default MainTitle;
