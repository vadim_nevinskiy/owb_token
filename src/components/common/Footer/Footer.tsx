import React from 'react';
import classes from "./Footer.module.scss";
import classNames from "classnames";



interface IProps {

}
const Footer:React.FC<IProps> = ({}) => {
    return (
        <div className={classNames({
            [classes.Footer]: true
        })}>
            <div className={classNames({
                [classes.Footer__wrapper]: true
            })}>
                <div className={classNames({
                    [classes.Footer__wrapper__FooterLogo]: true
                })} />
                <div className={classNames({
                    [classes.Footer__wrapper__copyText]: true
                })}>
                    © OWB-software limited, 2022
                </div>
                <div className={classNames({
                    [classes.Footer__wrapper__TermsLinks]: true
                })}>
                    <div>Terms of use</div>
                    <div>Privacy policy</div>
                </div>
            </div>
        </div>
    );
};

export default Footer;
