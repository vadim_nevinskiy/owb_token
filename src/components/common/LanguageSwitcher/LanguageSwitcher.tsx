import React, {useState} from 'react';
import classes from "./LanguageSwitcher.module.scss";
import classNames from "classnames";
import {languages} from "../../../data/languages";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../../store";

import {setLanguage} from '../../../store/reducers/main/mainSlice'

const LanguageSwitcher = () => {
    const dispatch = useDispatch();
    const [isActiveSwitcher, setIsActiveSwitcher] = useState(false);
    const selectedLanguage = useSelector((state: RootState) => state.main.language);

    const openSwitcher = () => {
        setIsActiveSwitcher(!isActiveSwitcher);
    }

    const updateLanguage = (selectedLang: string) => {
        dispatch(setLanguage(selectedLang));
        setIsActiveSwitcher(false);
    }

    return (
        <div className={classNames({
            [classes.LanguageSwitcher]: true,
        })}>
            <div
                className={classNames({
                    [classes.LanguageSwitcher__SelectedLanguage]: true,
                    [classes.LanguageSwitcher__SelectedLanguage_active]: isActiveSwitcher,
                })}
                onClick={openSwitcher}
            >
                <div className={classNames({
                    [classes.LanguageSwitcher__SelectedLanguage__Flag]: true,
                    [classes.LanguageSwitcher__Flag]: true,
                    [classes[`LanguageSwitcher__Flag_${selectedLanguage}`]]: true
                })}/>
            </div>
            <div className={classNames({
                [classes.LanguageSwitcher__Dropdown]: true,
                [classes.LanguageSwitcher__Dropdown_active]: isActiveSwitcher
            })}>
                {
                    languages
                        .filter(item => item.language !== selectedLanguage)
                        .map((item, _idx) => {
                        return (
                            <div
                                key={_idx}
                                className={classNames({
                                    [classes.LanguageSwitcher__Flag]: true,
                                    [classes[`LanguageSwitcher__Flag_${item.language}`]]: true,
                                })}
                                onClick={() => updateLanguage(item.language)}
                            />
                        )
                    })
                }
                {/*<div className={classNames({*/}
                {/*    [classes.LanguageSwitcher__Flag]: true,*/}
                {/*    [classes.LanguageSwitcher__Flag_zh]: true*/}
                {/*})} />*/}
                {/*<div className={classNames({*/}
                {/*    [classes.LanguageSwitcher__Flag]: true,*/}
                {/*    [classes.LanguageSwitcher__Flag_pl]: true*/}
                {/*})} />*/}
                {/*<div className={classNames({*/}
                {/*    [classes.LanguageSwitcher__Flag]: true,*/}
                {/*    [classes.LanguageSwitcher__Flag_ua]: true*/}
                {/*})} />*/}
                {/*<div className={classNames({*/}
                {/*    [classes.LanguageSwitcher__Flag]: true,*/}
                {/*    [classes.LanguageSwitcher__Flag_ru]: true*/}
                {/*})} />*/}

            </div>
        </div>
    );
};

export default LanguageSwitcher;
