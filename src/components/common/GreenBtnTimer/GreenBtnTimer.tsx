import React from 'react';
import classes from "./GreenBtnTimer.module.scss";
import classNames from "classnames";

interface IProps {

}
const GreenBtnTimer:React.FC<IProps> = ({}) => {
    return (
        <div className={classNames({
            [classes.GreenBtnTimer]: true
        })}>
            <div className={classNames({
                [classes.GreenBtnTimer__title]: true
            })}>
                whitelist now:
            </div>
            <div className={classNames({
                [classes.GreenBtnTimer__date]: true
            })}>
                <div className={classNames({
                    [classes.GreenBtnTimer__date_value]: true
                })}>
                    17
                </div>
                <div className={classNames({
                    [classes.GreenBtnTimer__date_label]: true
                })}>
                    d
                </div>

                <div className={classNames({
                    [classes.GreenBtnTimer__date_value]: true
                })}>
                    12
                </div>
                <div className={classNames({
                    [classes.GreenBtnTimer__date_label]: true
                })}>
                    h
                </div>

                <div className={classNames({
                    [classes.GreenBtnTimer__date_value]: true
                })}>
                    47
                </div>
                <div className={classNames({
                    [classes.GreenBtnTimer__date_label]: true
                })}>
                    m
                </div>
            </div>
        </div>
    );
};

export default GreenBtnTimer;
