import React from 'react';
import classes from "./Timer.module.scss";
import classNames from "classnames";

interface IProps {

}
const Timer:React.FC<IProps> = ({}) => {
    return (
        <div className={classNames({
            [classes.Timer]: true
        })}>
            <div className={classNames({
                [classes.Timer__Title]: true
            })}>
                update:
            </div>
            <div className={classNames({
                [classes.Timer__Date]: true
            })}>
                <div className={classNames({
                    [classes.Timer__Date_value]: true
                })}>
                    17
                </div>
                <div className={classNames({
                    [classes.Timer__Date_label]: true
                })}>
                    d
                </div>

                <div className={classNames({
                    [classes.Timer__Date_value]: true
                })}>
                    12
                </div>
                <div className={classNames({
                    [classes.Timer__Date_label]: true
                })}>
                    h
                </div>

                <div className={classNames({
                    [classes.Timer__Date_value]: true
                })}>
                    47
                </div>
                <div className={classNames({
                    [classes.Timer__Date_label]: true
                })}>
                    m
                </div>
            </div>
        </div>
    );
};

export default Timer;
