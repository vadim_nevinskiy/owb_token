import React from 'react';
import classNames from "classnames";
import classes from "./SubTitle.module.scss";

interface IProps {
    title: string
}
const SubTitle:React.FC<IProps> = ({title}) => {
    return (
        <div className={classNames({
            [classes.SubTitle]: true
        })}>
            {title}
        </div>
    );
};

export default SubTitle;
