import React, {useEffect, useRef} from 'react';
import classes from "./ChartVesting.module.scss";
import {data} from "../../../data/linechart";

import * as am5 from "@amcharts/amcharts5";
import * as am5xy from "@amcharts/amcharts5/xy";
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
import classNames from "classnames";
import * as am5percent from "@amcharts/amcharts5/percent";




interface IProps {

}
const ChartVesting:React.FC<IProps> = ({}) => {
    let chart = useRef<any>(null);




    useEffect(() => {
        am5.addLicense("AM5C355304082");
        const root = am5.Root.new("chart2");
        root.setThemes([
            am5themes_Animated.new(root)
        ]);
        if(!chart.current){
            chart.current = root.container.children.push(
                am5xy.XYChart.new(root, {
                    panY: false,
                    layout: root.verticalLayout
                })
            );

            let cursor = chart.current.set("cursor", am5xy.XYCursor.new(root, {
                behavior: "none"
            }));
            cursor.lineY.set("visible", false);


            let xAxis = chart.current.xAxes.push(am5xy.CategoryAxis.new(root, {
                categoryField: "grad",
                startLocation: 0.5,
                endLocation: 0.5,
                renderer: am5xy.AxisRendererX.new(root, {}),
                tooltip: am5.Tooltip.new(root, {})
            }));

            xAxis.data.setAll(data);

            let yAxis = chart.current.yAxes.push(am5xy.ValueAxis.new(root, {
                renderer: am5xy.AxisRendererY.new(root, {})
            }));



            // let legend = chart.current.children.push(am5.Legend.new(root, {
            //     centerX: am5.p50,
            //     x: am5.p50,
            //     layout: root.verticalLayout
            // }));

            let legend = chart.current.children.push(am5.Legend.new(root, {
                centerX: am5.percent(0),
                x: am5.percent(0),
                layout: am5.GridLayout.new(root, {
                    maxColumns: 3,
                    fixedWidthGrid: true
                })
            }));

            const createSeries = (name: string, field: any) => {
                let series = chart.current.series.push(am5xy.LineSeries.new(root, {
                    name: name,
                    xAxis: xAxis,
                    yAxis: yAxis,
                    stacked:true,
                    valueYField: field,
                    categoryXField: "grad",
                    tooltip: am5.Tooltip.new(root, {
                        pointerOrientation: "horizontal",
                        labelText: "[bold]{name}[/]\n{categoryX}: {valueY}"
                    })
                }));

                series.fills.template.setAll({
                    fillOpacity: 1,
                    visible: true
                });

                series.data.setAll(data);
                series.appear(1000);


                series.bullets.push(function() {
                    return am5.Bullet.new(root, {
                        locationY: 0.5,
                        sprite: am5.Label.new(root, {
                            text: "{valueY}",
                            fill: root.interfaceColors.get("alternativeText"),
                            centerY: am5.percent(50),
                            centerX: am5.percent(50),
                            populateText: true
                        })
                    });
                });

                legend.data.push(series);
            }



            createSeries("BCO", "value_1");
            createSeries("Reserve fund(Market Making and Marketing)", "value_2");
            createSeries("IDO(Community Sale)", "value_3");
            createSeries("Strategic Investor & Advisors, SAFT-NFT", "value_4");
            createSeries("Team, SAFT-NFT", "value_5");

            // series.appear();
            chart.current.appear();
        }

    }, [])




    return (
        <div className={classNames({
            [classes.ChartVesting]: true
        })}>
            <div className={classNames({
                [classes.ChartVesting__ChartContainer]: true
            })}>
                <div
                    id={'chart2'}
                    style={{
                        width: '100%',
                        height: '400px',
                        margin: '0px 0'
                    }}
                />
                <div id="legend2"></div>
            </div>
        </div>
    );
};

export default ChartVesting;
