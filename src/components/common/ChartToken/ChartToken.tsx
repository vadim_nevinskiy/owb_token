import React, {useEffect, useRef} from 'react';
import classes from "./ChartToken.module.scss";
import classNames from "classnames";


import * as am5 from "@amcharts/amcharts5";
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
import * as am5percent from "@amcharts/amcharts5/percent";



interface IProps {
    chartId?: string
}
const ChartToken:React.FC<IProps> = ({chartId}) => {
    let chart = useRef<any>(null);











    let data = [{
        name: "Community Development: Bounty",
        sales: 501.9,
        color: am5.color(0x297373)
    }, {
        name: "Community Development: Ambassadors",
        sales: 301.9,
        color: am5.color(0x297373)
    }, {
        name: "Community Development: Other",
        sales: 201.1,
        color: am5.color(0x297373)
    }, {
        name: "Reserve Funds",
        sales: 165.8,
        color: am5.color(0x297373)
    }, {
        name: "Legal",
        sales: 139.9,
        color: am5.color(0x297373)
    }, {
        name: "DEX Liquidity",
        sales: 128.3,
        color: am5.color(0x297373)
    }, {
        name: "Marketing",
        sales: 99,
        color: am5.color(0x297373)
    }, {
        name: "Prize Pools",
        sales: 60,
        color: am5.color(0x297373)
    }, {
        name: "Development",
        sales: 50,
        color: am5.color(0x297373)
    }, {
        name: "Research",
        sales: 50,
        color: am5.color(0x297373)
    }, {
        name: "Operations",
        sales: 50,
        color: am5.color(0x297373)
    }];


    useEffect(() => {
        am5.addLicense("AM5C355304082");
        const root = am5.Root.new("chart");
        root.setThemes([
            am5themes_Animated.new(root)
        ]);
        if(!chart.current){
            chart.current = root.container.children.push(
                am5percent.PieChart.new(root, {
                    radius: am5.percent(90),
                    innerRadius: am5.percent(50),
                    layout: root.horizontalLayout
                })
            );


            let series = chart.current.series.push(
                am5percent.PieSeries.new(root, {
                    name: "Series",
                    valueField: "sales",
                    categoryField: "name"
                })
            );




            series.labelsContainer.set("paddingTop", 30)



            series.get("colors", '').set("colors", [
                am5.color("#B11542"),
                am5.color("#B9236B"),
                am5.color("#DB66AA"),
                am5.color("#874CC0"),
                am5.color("#7A61D6"),
                am5.color("#5B63CE"),
                am5.color("#5B87CE"),
                am5.color("#519FC5"),
                am5.color("#78D0F8"),
                am5.color("#2DCCBA"),
                am5.color("#57A7CC"),
            ]);


            series.data.setAll(data);

            // Disable ticks and labels
            series.ticks.template.set("forceHidden", true);
            series.labels.template.set("forceHidden", true);


            // Disable tooltips
            series.slices.template.tooltipText = "";


            // Adding gradients
            series.slices.template.set("strokeOpacity", 0);
            series.slices.template.set("fillGradient",
                am5.RadialGradient.new(root, {
                stops: [{
                    brighten: -0.8
                }, {
                    brighten: -0.8
                }, {
                    brighten: -0.5
                }, {
                    brighten: 0
                }, {
                    brighten: -0.5
                }]
            }));




            // Create legend
            // https://www.amcharts.com/docs/v5/charts/percent-charts/legend-percent-series/
            var legend = root.container.children.push(am5.Legend.new(root, {
                layout: root.verticalLayout,
                centerX: am5.percent(50),
                x: am5.percent(75),

                nameField: "name",
                fillField: "color",
                strokeField: "color",
                useDefaultMarker: true
            }));

            // set value labels align to right
            legend.valueLabels.template.setAll({ textAlign: "right" })
            // set width and max width of labels
            legend.labels.template.setAll({
                maxWidth: 300,
                width: 300
            });
            legend.markerRectangles.template.setAll({
                cornerRadiusTL: 30,
                cornerRadiusTR: 30,
                cornerRadiusBL: 30,
                cornerRadiusBR: 30
            });





            legend.data.setAll(series.dataItems);

            series.appear();
            chart.current.appear();
        }

    }, [])


    return (
        <div className={classNames({
            [classes.ChartToken]: true
        })}>
            <div className={classNames({
                [classes.PieChart]: true
            })}>
                <div
                    id={'chart'}
                    style={{
                        width: '100%',
                        height: '400px',
                        margin: '0px 0'
                    }}
                />
                <div id="legend"></div>
            </div>
        </div>
    );
};

export default ChartToken;

