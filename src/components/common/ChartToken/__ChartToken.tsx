import React, {useEffect, useRef} from 'react';
import classes from "./ChartToken.module.scss";
import classNames from "classnames";


import * as am5 from "@amcharts/amcharts5";
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
import * as am5percent from "@amcharts/amcharts5/percent";




interface IProps {
    chartId?: string
}
const ChartToken:React.FC<IProps> = ({chartId}) => {
    let chart = useRef<any>(null);

    let data = [{
        value: 3,
        category: "Token 1"
    }, {
        value: 4,
        category: "Token 2"
    }, {
        value: 50,
        category: "Token 3"
    }];
    useEffect(() => {
        am5.addLicense("AM5C355304082");
        const root = am5.Root.new("chart");
        root.setThemes([
            am5themes_Animated.new(root)
        ]);
        if(!chart.current){
            chart.current = root.container.children.push(
                am5percent.PieChart.new(root, {
                    layout: root.verticalLayout,
                    radius: am5.percent(80),
                    // innerRadius: am5.percent(40)
                })
            );


            let series = chart.current.series.push(
                am5percent.PieSeries.new(root, {
                    alignLabels: true,
                    calculateAggregates: true,
                    valueField: "value",
                    categoryField: "category"
                })
            );




            series.labelsContainer.set("paddingTop", 30)

            //To added scaling sections by radius
            // series.slices.template.adapters.add("radius", function (radius: any, target: any) {
            //     let dataItem = target.dataItem;
            //     let high = series.getPrivate("valueHigh");
            //
            //     if (dataItem) {
            //         let value = target.dataItem.get("valueWorking", 0);
            //         return radius * value / high
            //     }
            //     return radius;
            // });


            ///////===///////
            // let colorSet = am5.ColorSet.new(root, {
            //
            //     colors: [Color.fromString("#cccccc")],
            //     // colors: [series.get("colors").getIndex(3)],
            //     // passOptions: {
            //     //     lightness: -0.05,
            //     //     hue: 0
            //     // }
            // });
            // series.set("colors", colorSet);



            series.get("colors", '').set("colors", [
                am5.color("#8a8a8a"),
                am5.color("#eeeeee"),
                am5.color("#acacac"),
            ]);


            series.data.setAll(data);

            // Disable ticks and labels
            series.ticks.template.set("forceHidden", true);
            series.labels.template.set("forceHidden", true);


            // Disable tooltips
            series.slices.template.tooltipText = "";



            series.appear();
            chart.current.appear();
            // series.slices.template.setAll({
            //     strokeWidth: 1,
            //     stroke: am5.color(0xffffff)
            // });

            // series.set("fill", am5.color(0xff0000)); // Not Working!!!


            // Add a legend
            // chart.current.legend = new am4charts.Legend();




        }

    }, [])


    return (
        <div className={classNames({
            [classes.ChartToken]: true
        })}>
            <div className={classNames({
                [classes.PieChart]: true
            })}>
                <div
                    id={'chart'}
                    style={{
                        width: '100%',
                        height: '400px',
                        margin: '0px 0'
                    }}
                />
                <div id="legend"></div>
            </div>
        </div>
    );
};

export default ChartToken;

