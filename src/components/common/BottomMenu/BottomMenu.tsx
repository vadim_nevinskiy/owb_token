import React from 'react';
import classes from "./BottomMenu.module.scss";
import classNames from "classnames";
import {Menu, SocialNetwork} from "../index";


interface IProps {

}
const BottomMenu:React.FC<IProps> = ({}) => {
    return (
        <div className={classNames({
            [classes.BottomMenu]: true
        })}>
            <div className={classNames({
                [classes.BottomMenu__wrapper]: true
            })}>
                <Menu mobileVersion={false} isBottomMenu={true} />
                <SocialNetwork />
            </div>
        </div>
    );
};

export default BottomMenu;
