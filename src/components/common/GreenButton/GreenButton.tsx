import React from 'react';
import classes from "./GreenButton.module.scss";
import classNames from "classnames";



interface IProps {
    title: string
    clickBtn: () => void
    sizeBtn?: 'small'
    // sizeBtn: 'small' | 'middle' | 'large'
    condition?: 'Default' | 'Active' | 'Disabled'
}
const GreenButton:React.FC<IProps> = ({title, clickBtn, sizeBtn, condition = 'Default'}) => {
    return (
        <div
            className={classNames({
                [classes.GreenButton]: true,
                [classes.GreenButton__small]: sizeBtn === 'small',
                // [classes[`GreenButton__${sizeBtn}`]]: true
                [classes[`GreenButton__Condition${condition}`]]: true
            })}
            onClick={condition !== 'Disabled' ? clickBtn : () => {} }
        >
            {title}
        </div>
    );
};

export default GreenButton;
