import React from 'react';
import {Content, FourthScreenContentTable, IndentAdjustmentBlockForAnchors, MainTitle, SubTitle} from "../common";
import classNames from "classnames";
import classes from "./ScreenFour.module.scss";
import useTranslation from "../../hooks/useTranslation";


interface IProps {

}

const ScreenFour:React.FC<IProps> = ({}) => {
    const translation = useTranslation('TokenLanding');



    return (
        <div
            className={classNames({
                [classes.ScreenFour]: true
            })}
        >
            <IndentAdjustmentBlockForAnchors topPosition={80} blockId={"ScreenFour"} />
            <div className={classNames({
                [classes.ScreenFour__Wrapper]: true
            })}>
                <SubTitle title={translation("FourthScreenSubTitle")} />
                <MainTitle title={translation("FourthScreenMainTitle")} />
                <div className={classNames({
                    [classes.ScreenFour__Wrapper__ContentBlock]: true
                })}>
                    <Content html={translation("FourthScreenContent")} />
                </div>
                <FourthScreenContentTable />
            </div>
            <div className={classNames({
                [classes.ScreenFour__BottomEllipse]: true
            })} />
        </div>
    );
};

export default ScreenFour;
