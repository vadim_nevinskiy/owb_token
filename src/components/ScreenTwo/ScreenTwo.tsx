import React from 'react';
import classNames from "classnames";
import classes from "./ScreenTwo.module.scss";
import {
    Content,
    GreenButton, IndentAdjustmentBlockForAnchors,
    MainTitle,
    ScreenTwoContentTable,
    SubTitle,
    Timer
} from "../common";
import useTranslation from "../../hooks/useTranslation";





interface IProps {

}
const ScreenTwo:React.FC<IProps> = ({}) => {
    const translation = useTranslation('TokenLanding');


    const content = {
        leftColumn: {text_1: translation("SecondScreenLeftColumn_1"), text_2: translation("SecondScreenLeftColumn_2")},
        rightColumn: {text_1: translation("SecondScreenRightColumn_1"), text_2: translation("SecondScreenRightColumn_2")}
    }

    return (
        <div
            className={classNames({
                [classes.ScreenTwo]: true
            })}
        >
            <IndentAdjustmentBlockForAnchors topPosition={-100} blockId={"ScreenTwo"} />
            <div className={classNames({
                [classes.ScreenTwo__wrapper]: true
            })}>
                <div className={classNames({
                    [classes.ScreenTwo__wrapper__BgLight]: true
                })}>
                    <SubTitle title={translation("SecondScreenSubTitle")} />
                    <MainTitle title={translation("SecondScreenMainTitle")} />

                    <ScreenTwoContentTable
                        leftColumn={content.leftColumn}
                        rightColumn={content.rightColumn} />
                </div>
            </div>
            <div className={classNames({
                [classes.ScreenTwo__BottomRadiusBlock]: true
            })} />
        </div>
    );
};

export default ScreenTwo;
