export { default as Header } from "./Header/Header";
export { default as ScreenOne } from "./ScreenOne/ScreenOne";
export { default as ScreenTwo } from "./ScreenTwo/ScreenTwo";
export { default as ScreenThree } from "./ScreenThree/ScreenThree";
export { default as ScreenFour } from "./ScreenFour/ScreenFour";
export { default as ScreenFive } from "./ScreenFive/ScreenFive";
export { default as ScreenSix } from "./ScreenSix/ScreenSix";
export { default as ScreenSeven } from "./ScreenSeven/ScreenSeven";
