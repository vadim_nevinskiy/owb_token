export async function getDataFromUrl(url: string | undefined) {
    if(url){
        const res = await fetch(url);
        const body = await res.json();
        if (!body) {
            throw new Error('API fetch error');
        }
        return body;
    }
    return
}
