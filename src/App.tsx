import React, {useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import {MainPage, NotFound} from "./pages";
import {
    setLocalization,
} from './store/reducers/cdnLocalization/cdnLocalizationSlices';
import {setLanguage} from "./store/reducers/main/mainSlice";
import {useDispatch} from "react-redux";
import fetchCdnLocalizationService from "./services/fetchCdnLocalizationService";
import dotObject from 'dot-object';
import detectLanguage from "./utils/detectLanguage";


function App() {
    const dispatch = useDispatch();

    useEffect(() => {
        fetchCdnLocalizationService()
            .then(Object.entries)
            .then((entries: [string, any][]) => {
                const translation = Object.fromEntries(
                    entries.map(([language, fields]) => {
                        return (
                            [
                                language,
                                dotObject.object(fields),
                            ]
                        )
                    })
                );
                dispatch(setLocalization(translation));
            })
    })
    useEffect(() => {
        const lang = localStorage.getItem('lng') || detectLanguage();
        dispatch(setLanguage(lang));
    }, [dispatch]);


  return (
      <BrowserRouter>
        <div className="App">
            <Routes>
                <Route path="/" element={<MainPage />} />
                <Route path="*" element={<NotFound />} />
            </Routes>
        </div>
      </BrowserRouter>
  );
}

export default App;
